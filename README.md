# NGCLIB helper scripts

This repository holds various generic scripts used to train, test or evaluate NGC models.

Each NGC model is conceptually identical, except the low level nodes (`NGCNode`). The define:
- how the network edges are instantiated: via `get_encoder` and `get_decoder`
- what node-specific metrics are to be used (offline, online and criterion)
- how the data is handled when read from the disk

The edges are created by linking nodes between eachother using the `GraphCfg` object, read from a `.yaml` file. The NGC Model is defined by putting all the edges together.

Generally, these scripts require just a `/path/to/ngc/nodes`, which is a module containing `__init__.py` and all the `nodes'` implmenetation. It should look something like this:

```
/path/to/ngc/nodes
  __init__.py # Exports nodes RGB, Depth and Semantic
  rgb.py # Implements node RGB
  depth.py # Implements node Depth
  semantic.py # Implements node Semantic
```

In our scripts, we'll use the helper method:
```
graph_cfg = GraphCfg("/path/to/graph.yaml") # defines node types that are to be imported (RGB, Depth, Semantic etc.)
nodes = graph_cfg.get_nodes_from_module("/path/to/ngc/nodes") # same path as above
```

Then, all our models can be instantiated properly:
```
ngc_model: NGC = NGCV1(nodes, graph_cfg.cfg)
```
