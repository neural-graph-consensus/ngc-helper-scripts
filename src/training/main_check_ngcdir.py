#!/usr/bin/env python3
# Script used to get a status of a ngc dir (training status for now).
import yaml
from argparse import ArgumentParser
from pathlib import Path
from ngclib.ngcdir import NGCDir
from ngclib.graph_cfg import GraphCfg

def get_args():
    parser = ArgumentParser()
    parser.add_argument("ngc_dir_path", type=lambda p: Path(p).absolute())
    parser.add_argument("graph_cfg_path", type=Path)
    parser.add_argument("--json", action="store_true")
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    graph_cfg = GraphCfg(yaml.safe_load(open(args.graph_cfg_path, "r"))["graph"])
    ngc_dir = NGCDir(args.ngc_dir_path, graph_cfg)
    res = ngc_dir.status if not args.json else ngc_dir.to_json()
    print(res)

if __name__ == "__main__":
    main()
