# Script that takes an output directory of predictions (vote + single links) and makes a collage, based on the node
#  node type. Used for visualization.
# Example command: Name=depthEnsemble; rm -rf testCollage/$Name/; python main_make_collage.py  --predictionsDir others/results_20210929_slanic/out_slanic_mean/$Name/ --graphCfgPath cfgs/graph/slanic_complete.yaml --nodeName $Name --outputDir testCollage/$Name --rgbDir /scratch/nvme2n1/Datasets/AAAI2022/dataset/representations/slanic_combined_sliced_780_9780_split/validation/rgb/ --gtDir /scratch/nvme2n1/Datasets/AAAI2022/dataset/representations/slanic_combined_sliced_780_9780_split/validation/sfm/
# For video: cd testCollage/depthEnsemble; ffmpeg -start_number 0 -framerate 10 -i %d.png -c:v libx264 -pix_fmt yuv420p depthEnsemble_collage.mp4
from typing import List
from natsort import natsorted
import yaml
import numpy as np
import shutil
from argparse import ArgumentParser
from pathlib import Path
from media_processing_lib.collage_maker import CollageMaker
from media_processing_lib.video import MPLVideo
from media_processing_lib.video.backends import DiskBackend
from ngclib.utils import load_npz
from ngclib_cv.nodes import *
from ngclib_cv.data import build_plot_function
from ngclib.utils import GraphCfg

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--collageCfgPath", required=True)
    parser.add_argument("--outputDir", required=True, help="Where to output the PNG files of the collage")
    parser.add_argument("--nodes_path", required=True)
    parser.add_argument("--overwrite", type=int, default=0)
    parser.add_argument("--do_video", type=int, default=1)
    parser.add_argument("--end_ix", type=int)
    parser.add_argument("--resolution", type=int, nargs="+")
    args = parser.parse_args()
    args.collageCfg = yaml.safe_load(open(args.collageCfgPath, "r"))
    args.outputDir = Path(args.outputDir).absolute()
    args.overwrite = bool(args.overwrite)
    if args.overwrite and args.outputDir.exists():
        shutil.rmtree(str(args.outputDir))
    args.do_video = bool(args.do_video)
    assert args.resolution is None or len(args.resolution) == 2
    return args

def getFiles(inputDirs: List[str]) -> List[List[Path]]:
    res = []
    firstFound = None
    for dir in inputDirs:
        if dir is not None:
            firstFound = dir
            break
    assert firstFound is not None

    for dir in inputDirs:
        if dir is None:
            dir = firstFound
        dir = Path(dir)
        assert dir.exists(), dir
        files = [Path(x) for x in natsorted([str(x) for x in dir.glob("*.npz")])]
        assert len(files) > 0
        res.append(files)
    assert np.std([len(x) for x in res]) == 0, [len(x) for x in res]
    return res

def main():
    args = getArgs()
    cfg = args.collageCfg
    nodes = GraphCfg(cfg).get_nodes_from_module(args.nodes_path)
    plot_fns = [build_plot_function(node) for node in nodes]
    files = getFiles(cfg["inputDirs"])
    titles = cfg["titles"]
    rows_cols = None if not "rowsCols" in cfg else cfg["rowsCols"]
    collageMaker = CollageMaker(files=files, plot_fns=plot_fns,
                                rows_cols=rows_cols, output_dir=args.outputDir,
                                names=titles, load_fns=load_npz, resolution=args.resolution)
    print(collageMaker)
    collageMaker.make_collage(end_ix=args.end_ix)

    if args.do_video:
        files = [x for x in Path(args.outputDir).iterdir()]
        video = MPLVideo(DiskBackend(files), fps=30)
        video_out_path = Path(f"{args.outputDir}.mp4")
        video.write(video_out_path)
        print(f"Video saved at '{video_out_path}' !")

if __name__ == "__main__":
    main()
