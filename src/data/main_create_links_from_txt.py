import os
from pathlib import Path
from tqdm import trange
from argparse import ArgumentParser

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--baseDirectory", required=True, help="Root dir where the files from txt are located")
    parser.add_argument("--txtFilePath", required=True, help="The txt file that describes the npz files")
    parser.add_argument("--outDirectory", required=True, help="Path where the links are to be stored")
    parser.add_argument("--representations", required=True, help="List of representations, delimited by ','")
    args = parser.parse_args()
    args.baseDirectory = Path(args.baseDirectory).absolute()
    args.txtFilePath = Path(args.txtFilePath).absolute()
    args.outDirectory = Path(args.outDirectory).absolute()
    args.representations = args.representations.split(",")
    return args

def checkArgs(args):
    Dirs = []
    for potentialDir in args.baseDirectory.iterdir():
        # if potentialDir.name == args.txtFilePath.parents[0].name:
        #     continue
        good = True
        for representation in args.representations:
            if not (potentialDir / representation).exists():
                good = False
                break
        if good:
            Dirs.append(potentialDir.name)
    print(f"Directories to sample from: {Dirs}")

def main():
    args = getArgs()
    print(f"Base directory: '{args.baseDirectory}'.\nTxt file: '{args.txtFilePath}'.\n"
          f"Out directory: '{args.outDirectory}'.\nRepresentations: {args.representations}")
    filesList = open(args.txtFilePath, "r").readlines()
    if len(filesList[0].split("/")) == 2:
        checkArgs(args)

    # Create the directories for all representations in outdir.
    args.outDirectory.mkdir(exist_ok=True, parents=True)
    for representation in args.representations:
        (args.outDirectory/representation).mkdir(exist_ok=False)

    for i in trange(len(filesList)):
        file = filesList[i].strip()
        # Either sceneName/X(.npz) or just X(.npz)
        split = file.split("/")
        if len(split) == 2:
            inDir = args.baseDirectory / split[0]
        else:
            inDir = args.baseDirectory
        fileName = int(split[-1])
        for representation in args.representations:
            try:
                inPath = inDir/representation/f"{fileName}.npz"
                assert inPath.exists(), inPath
            except:
                inPath = inDir/representation/f"{fileName:06d}.npz"
                assert inPath.exists(), inPath
            outPath = args.outDirectory/representation/f"{i}.npz"
            os.symlink(inPath, outPath)
    print("Done!")

if __name__ == "__main__":
    main()
