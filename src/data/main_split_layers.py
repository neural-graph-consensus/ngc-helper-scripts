# Simple script to split a full layers directory into 3 directories:
# Data is split by Train%,Validation%,Semisupervised%
# The values should sum to 100
# Train and Validation dirs should use all layers
# Semisupervised dir should use only rgb

import os
import numpy as np
from pathlib import Path
from argparse import ArgumentParser
from typing import Dict, List
from natsort import natsorted

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--layers_dir", required=True)
    parser.add_argument("-o", "--out_dir", required=True)
    parser.add_argument("--percentages", required=True)
    parser.add_argument("--skip", type=int, default=0)
    parser.add_argument("--input_nodes", default=None, nargs="+")
    args = parser.parse_args()
    args.percentages = [float(x) for x in args.percentages.split(",")]
    args.layers_dir = Path(args.layers_dir).absolute()
    args.out_dir = Path(args.out_dir).absolute()
    if args.input_nodes is None:
        args.input_nodes = [x.name for x in args.layers_dir.iterdir() if x.is_dir()]
    assert not args.out_dir.exists()
    return args

# Checks if all npz files in each layer directory has the same number of items.
def checklayers_dir(input_nodes: List[str], layers_dir: Path):
    all_layer_dirs = [x for x in filter(lambda y: y.is_dir() and y.name != "collage", layers_dir.iterdir())]
    allLayerFiles = [[x for x in Dir.glob("*.npz")] for Dir in all_layer_dirs]
    Lens = [len(x) for x in allLayerFiles]
    all_layer_dirs = [x.name for x in all_layer_dirs]
    print(f"[checklayers_dir] Found layers: {all_layer_dirs}")
    assert np.std(Lens) == 0, dict(zip(all_layer_dirs, Lens))
    for inputNode in input_nodes:
        assert inputNode in all_layer_dirs, f"{inputNode} not in {all_layer_dirs}"

# Takes one layer_dir and the percentages and returns the files to be kept in each of the 3 independent layer dirs that
#  are going to be created (Train, Validation and semisupervised). Semisupervised one keeps only RGB.
def split_files(input_nodes: List[str], layers_dir: Path, percentages: List[float], skip: int) -> Dict[str, List[str]]:
    all_layer_dirs = [x for x in filter(lambda y: y.is_dir() and y.name != "collage", layers_dir.iterdir())]
    allLayerFiles = [natsorted([x for x in Dir.glob("*.npz")], key=lambda path: path.name) for Dir in all_layer_dirs]
    allLayerFiles = [x[skip :] for x in allLayerFiles]
    Len = len(allLayerFiles[0])
    split_lens = [int(x * Len / 100) for x in percentages]
    split_lens[-1] = Len - split_lens[0] - split_lens[1]
    cum_lens = np.cumsum(split_lens)
    print(f"[split_files] Input nodes: {input_nodes}")
    print(f"[split_files] Lens: {split_lens}")

    all_layer_dirs = [x.name for x in all_layer_dirs]
    input_nodesIndexes = [all_layer_dirs.index(x) for x in input_nodes]
    trainFiles = [x[0 : cum_lens[0]] for x in allLayerFiles]
    validationFiles = [x[cum_lens[0] : cum_lens[1]] for x in allLayerFiles]
    semisupervisedFiles = [allLayerFiles[x][cum_lens[1] : ] for x in input_nodesIndexes]

    trainRes = {k: v for k, v in zip(all_layer_dirs, trainFiles)}
    validationRes = {k: v for k, v in zip(all_layer_dirs, validationFiles)}
    semisupervisedRes = {k: v for k, v in zip(input_nodes, semisupervisedFiles)}
    return {"train": trainRes, "validation": validationRes, "semisupervised": semisupervisedRes}

def main():
    args = getArgs()
    checklayers_dir(args.input_nodes, args.layers_dir)
    files = split_files(args.input_nodes, args.layers_dir, args.percentages, args.skip)

    args.out_dir.mkdir(exist_ok=False, parents=True)
    # topLevel == train/validation/semisupervised
    for topLevel in files.keys():
        topLevelDir = args.out_dir / topLevel
        topLevelDir.mkdir()
        # layer == rgb/depth/semantic etc.
        for layer in files[topLevel].keys():
            layer_dir = args.out_dir / topLevel / layer
            layer_dir.mkdir()
            # file == 0.npz, 1.npz, ..., N.npz
            for file in files[topLevel][layer]:
                outFile = layer_dir / file.name
                os.symlink(file, outFile)
    print(f"Exported to '{args.out_dir}'")

if __name__ == "__main__":
    main()
