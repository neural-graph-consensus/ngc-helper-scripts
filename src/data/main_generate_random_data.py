from pathlib import Path
from argparse import ArgumentParser
from ngclib.utils import generate_random_data
from ngclib.logger import logger


def get_args():
    parser = ArgumentParser()
    parser.add_argument("--names", required=True, nargs="+", help="The K names of the output representations.")
    parser.add_argument(
        "--types",
        required=True,
        nargs="+",
        help="The K types of the output representations. " "Either one or K entries must be provided",
    )
    parser.add_argument(
        "--dims",
        required=True,
        nargs="+",
        type=int,
        help="The D dimensions of the last channel of "
        "the K representations. Either one or K entries must be provided",
    )
    parser.add_argument(
        "--shape", required=True, nargs="+", type=int, help="Shape for all representations, except last dim"
    )
    parser.add_argument(
        "-n", "--num_items", required=True, nargs="+", type=int, help="How many of each type. Either one number of K"
    )
    parser.add_argument("-o", "--output_dir", required=True)
    parser.add_argument("--overwrite", action="store_true")
    args = parser.parse_args()
    args.output_dir = Path(args.output_dir).absolute()
    k = len(args.names)
    args.types = k * [args.types[0]] if len(args.types) == 1 else args.types
    args.dims = k * [args.dims[0]] if len(args.dims) == 1 else args.dims
    args.num_items = k * [args.num_items[0]] if len(args.num_items) == 1 else args.num_items
    assert len(args.names) == len(args.types) == len(args.dims) == k, f"Expected {k}"
    assert len(args.shape) > 0, args.shape
    for s in args.shape:
        assert s > 0, f"Expected a positive shape, got {s} in {args.shape}"
    for t in args.types:
        assert t in ("float", "categorical"), f"Expected 'float' or 'categorical', got {t}"
    return args


def main():
    args = get_args()
    logger.info(
        f"""
[NGC Generate data]
- Output dir '{args.output_dir}'.
- Names: {args.names}
- Types: {args.types}
- Dims: {args.dims}
- Shape: {args.shape}
- Nums: {args.num_items}
- Overwrite: {args.overwrite}
"""
    )

    generate_random_data(args.output_dir, args.names, args.dims, args.types, args.shape, args.num_items, args.overwrite)
    exit()
    print("[NGC::Generate Random Data] Done!")
    # generate(args.baseDir / "train", args.representations)
    # generate("random/train", N=80)
    # generate("random/validation", N=20)
    # print("Done!")


if __name__ == "__main__":
    main()
