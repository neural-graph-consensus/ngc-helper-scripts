#!/usr/bin/env python3
from functools import partial
from pathlib import Path
from typing import List, Tuple
from argparse import ArgumentParser
from omegaconf import OmegaConf
import yaml
import os
import torch as tr
import numpy as np

from tqdm import tqdm
from nwutils.torch import tr_to_device
from torch.utils.data import DataLoader
from ngclib.logger import logger
from ngclib.readers import NGCNpzReader
from ngclib.graph_cfg import GraphCfg, NGCNodesImporter
from ngclib.models import NGC, NGCNode
from ngclib.utils import dump_predictions
from media_processing_lib.image import image_write
from simple_caching.storage import DictMemory

from models import get_model_type


def get_args():
    parser = ArgumentParser()
    parser.add_argument("--dataset_path", required=True)
    parser.add_argument("--mode", required=True, help="semisupervised (no gt expected) or test (gt is also there)")
    parser.add_argument("--config_path", required=True, help="Path to the data & graph config file")
    parser.add_argument("--weights_ngc_dir_path", required=True, help="Path to models dir of the desired iteration")
    parser.add_argument("--nodes_path", required=True)
    parser.add_argument("--output_path", "-o", required=True)
    # parser.add_argument("--batch_size", type=int, default=5)
    # parser.add_argument("--num_workers", type=int, default=0)
    # parser.add_argument("--dump_png", action="store_true")
    args = parser.parse_args()
    args.output_path = Path(args.output_path).absolute()
    args.weights_ngc_dir_path = Path(args.weights_ngc_dir_path).absolute()
    assert args.mode in ("semisupervised", "test")
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.graph_cfg_path)
    graph_cfg = GraphCfg(cfg.graph)
    nodes = NGCNodesImporter.from_graph_cfg(graph_cfg, nodes_module_path=args.nodes_path).nodes
    logger.info(f"Dump predictions."
                f"\n- Dataset path: '{args.dataset_path}."
                f"\n- Graph cfg: '{args.graph_cfg_path}'"
                f"\n- Nodes: {graph_cfg.node_names}"
                f"\n- Weights dir: '{args.weights_ngc_dir_path}'."
                f"\n- Out path: '{args.output_path}'")

    out_nodes = [node.name for node in nodes if node.name in graph_cfg.output_nodes]
    if args.mode == "semisupervised":
        _nodes = [node for node in nodes if node not in out_nodes]
        reader = NGCNpzReader(path=args.dataset_path, nodes=_nodes, out_nodes=[])
    else:
        reader = NGCNpzReader(path=args.dataset_path, nodes=nodes, out_nodes=out_nodes)
    # dataloader_params = {"batch_size": args.batch_size, "num_workers": args.num_workers, "shuffle": False,
    #                      "collate_fn": reader.collate_fn, "num_workers": 0}
    dataloader = DataLoader(reader, **cfg.data.loader_params, collate_fn=reader.collate_fn)

    vote_fn = lambda votes, sources: votes.median(axis=1)[0]
    edge_model_fn = partial(get_model_type, str_model_type="safeuav")
    graph = graph_cfg.build_model(nodes, edge_model_fn, vote_fn).to("cpu")
    print(graph)

    graph.eval()
    graph.load_all_weights(args.weights_ngc_dir_path)
    dump_predictions(graph, dataloader, reader.out_nodes, args.output_path)

if __name__ == "__main__":
    main()
