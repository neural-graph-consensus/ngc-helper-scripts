"""From per-data-item to dataset summary of an loco export"""
from pathlib import Path
from typing import Optional, List, Dict
from argparse import ArgumentParser
from functools import partial
import ast
import pandas as pd
import numpy as np
from torchmetrics.functional.classification.f_beta import _fbeta_compute
from nwutils.torch import tr_get_data
from ngclib.logger import logger
from ngclib.evaluation.node_metrics_loco import _format_metrics


def get_args():
    parser = ArgumentParser()
    parser.add_argument("df_path", help="Path to exported .csv containing for per-item metric results")
    parser.add_argument("type", help="Regression or classification")
    parser.add_argument("--split_index", help="Path to .txt file to split output indexes from")
    parser.add_argument("--out_file", "-o")
    parser.add_argument("--overwrite", action="store_true")
    parser.add_argument("--classification_type", default="f1")
    args = parser.parse_args()
    assert args.type in ("regression", "classification")
    return args


def fix_df(df):
    """Convert from list strings to actual lists, and add nans back because of ast.literal_eval error"""
    for col in df.columns:
        if "class" in col:

            def f(row):
                row = row.replace("nan", "-9999")
                row = ast.literal_eval(row)
                row = np.array(row)
                row[row == -9999] = np.nan
                return row

            df[col] = df[col].apply(f)
    return df


def get_means(df):
    """Get means of all numerical values, be them per-class (lists) or global (numbers)"""
    means = {}
    for col in df.columns:
        if col == "scene":
            continue
        if "class" in col:
            arr = np.stack(df[col])
            res = np.nanmean(arr, axis=0).tolist()
        elif "global" in col:
            arr = np.stack(df[col])
            res = np.nanmean(arr)
        else:
            res = df[col].iloc[0]
        means[col] = res
    return means


def split_dfs(df, split_index_path):
    """Split the df in multiple dfs per-scene based on the .txt file defining the splits"""
    split_index = open(split_index_path, "r").readlines()
    split_index = list(filter(lambda x: x != "\n", split_index))
    split_index = list(map(lambda x: x.strip("\n"), split_index))
    assert len(split_index) == len(
        df
    ), f"Split index file '{split_index_path}' contains only {len(split_index)} lines. Df: {len(df)}"
    split_index = [x.split("/")[0] for x in split_index]
    split_index = dict(zip(range(len(split_index)), split_index))
    df["scene"] = [split_index[ix] for ix in df.index]
    split_df = [x[1] for x in df.groupby("scene")]
    return split_df


def get_f1s_per_class(stats: np.ndarray):
    stats = stats.reshape(-1, 4).T
    tp, fp, tn, fn = tr_get_data(stats)
    f1s = []
    for _tp, _fp, _tn, _fn in zip(tp, fp, tn, fn):
        f1_class = _fbeta_compute(
            _tp, _fp, _tn, _fn, beta=1, ignore_index=None, average="macro", mdmc_average=None
        ).item()
        f1s.append(f1_class)
    return f1s


def get_accs_per_class(stats: np.ndarray):
    stats = stats.reshape(-1, 4).T
    tp, fp, tn, fn = stats
    accs = []
    for _tp, _fp, _tn, _fn in zip(tp, fp, tn, fn):
        acc_class = _tp / (_tp + _fp)
        accs.append(float(acc_class))
    return accs


def split_res(res: List) -> Dict[int, List]:
    # Make a dict {class_ix => [value]}. We need a list, so pd.DataFrame puts it on a new row.
    x = range(len(res))
    y = [[_x] for _x in res]
    split_x = dict(zip(x, y))
    return split_x


def df_reduce_classification(df: pd.DataFrame, which: str) -> pd.Series:
    assert which in ("f1", "accuracy")
    df["stat_scores"] = df["stat_scores"].apply(ast.literal_eval).apply(np.array)
    sums = df["stat_scores"].sum(0)
    scene = df["scene"].iloc[0]

    if which == "f1":
        result_from_sums = get_f1s_per_class(sums)
    else:
        result_from_sums = get_accs_per_class(sums)
    res = {"scene": scene, **split_res(result_from_sums)}
    res = pd.DataFrame(res).iloc[0]
    return res


def df_reduce_regression(df: pd.DataFrame) -> pd.Series:
    scene = df["scene"].iloc[0]
    means = get_means(df)
    means = pd.Series({"scene": scene, **means})
    return means


def get_average_scene_result(all_scenes: List[pd.Series]) -> pd.Series:
    logger.info("Split index provided. Calculating overall means for all scenes combined.")
    all_scenes_numerical = [x.drop("scene").values.astype(np.float32) for x in all_scenes]
    all_scenes_mean_by_class = np.array(all_scenes_numerical).mean(axis=0)
    all_means = ["all", *all_scenes_mean_by_class]
    all_mean_series = pd.Series(all_means)
    all_mean_series = all_mean_series.set_axis(tuple(all_scenes[0].axes[0]))
    return all_mean_series


def format_df(res: pd.DataFrame):
    for col in res.columns:
        if res[col].dtype == float:
            res[col] = res[col].apply(_format_metrics)
    return res


def df_reduce(
    df_path: Path,
    type: str,
    split_index: Optional[Path],
    out_file: Optional[Path] = None,
    overwrite: bool = False,
    classification_type: str = "f1",
):
    assert type in ("regression", "classification")
    df_path = Path(df_path).absolute()
    df = pd.read_csv(df_path, index_col=0)

    if split_index is None:
        logger.info("No splitting of scenes.")
        df["scene"] = "all"
        dfs = [df]
    else:
        logger.info(f"Splitting scenes by split index: '{split_index}'")
        dfs = split_dfs(df.copy(), split_index)

    reduce_fn = (
        partial(df_reduce_classification, which=classification_type)
        if type == "classification"
        else df_reduce_regression
    )
    res = []
    for scene_df in dfs:
        scene_reduced = reduce_fn(scene_df)
        res.append(scene_reduced)

    if split_index is not None:
        res_avg = get_average_scene_result(res)
        # Add also the result for no split index
        res.append(res_avg)
        df["scene"] = "all_no_scene_index"
        all_no_scene = reduce_fn(df)
        res.append(all_no_scene)
    res = pd.DataFrame(res).reset_index(drop=True)

    if out_file is None:
        out_file = str(df_path.parent / df_path.name.split(".")[0])
        if type == "classification":
            out_file += f"_{classification_type}"
        out_file += "_reduced.csv"
        logger.info(f"Out file not provided. Using in file as base path: '{out_file}'")

    if not overwrite:
        assert not Path(out_file).exists(), f"Path '{out_file}' exists. Use --overwrite."

    res = format_df(res)
    res.to_csv(out_file)
    print(f"Saved to '{out_file}'")


def main():
    args = get_args()
    df_reduce(args.df_path, args.type, args.split_index, args.out_file, args.overwrite, args.classification_type)


if __name__ == "__main__":
    main()
